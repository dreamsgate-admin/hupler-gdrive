import os
import pickle
import json

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from apiclient import errors

import consts


def get_service():
    """Shows basic usage of the Drive v3 API.
    Prints the names and ids of the first 10 files the user has access to.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', consts.SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('drive', 'v3', credentials=creds)
    return service


def get_production_data_folder_ids(service, production_folders):
    production_data_folder_ids = {}

    page_token = None
    while True:
        response = service.files().list(
            q="mimeType = 'application/vnd.google-apps.folder'",
            spaces='drive',
            fields='nextPageToken, files(id, name)',
            pageToken=page_token
        ).execute()

        for file in response.get('files', []):
            if file.get('name') in production_folders:
                production_data_folder_ids[file.get('name')] = file.get('id')

        page_token = response.get('nextPageToken', None)
        if page_token is None:
            break
    if production_data_folder_ids is None:
        raise Exception(
            "Couldn't find folders with prod names, ",
            production_folders
        )
    return production_data_folder_ids


def get_files_in_drive_folder(service, folder_id):
    """Print files belonging to a folder.

    Args:
        service: Drive API service instance.
        folder_id: ID of the folder to print files from.
    """
    page_token = None
    files = []
    while True:
        try:
            param = {}
            if page_token:
                param['pageToken'] = page_token
            response = service.files().list(
                q=f"'{folder_id}' in parents",
                spaces='drive',
                fields='nextPageToken, files(id, name)',
                pageToken=page_token
            ).execute()
            files.extend(response['files'])
            page_token = response.get('nextPageToken', None)
            if not page_token:
                break
        except errors.HttpError as error:
            print('An error occurred: %s' % error)
            break
    return files


def get_config_data():

    config_file_path = os.path.join('config.json')

    with open(config_file_path, 'r', encoding='utf-8') as fp:
        upload_config = json.load(fp)
        return upload_config
