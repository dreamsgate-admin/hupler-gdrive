import os
import glob


def clean_json_data_folder(delete_temp_only=False):
    if not delete_temp_only:
        print("Cleaning json and csv data files...")
        for path in glob.glob('json_data/*.json'):
            os.unlink(path)
        for path in glob.glob('json_data/*.csv'):
            os.unlink(path)
    print("Cleaning temp files...")
    for path in glob.glob('json_data/temp/*.json'):
        os.unlink(path)
    print("Done")


def main():
    answer = ''
    while(answer not in ['y', 'n']):
        answer = input("Delete temp folder only? [y/n]")
    if answer == 'y':
        delete_temp_only = True
    if answer == 'n':
        delete_temp_only = False

    clean_json_data_folder(delete_temp_only)


if __name__ == "__main__":
    main()
