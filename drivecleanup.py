from utils import get_service, get_production_data_folder_ids
from utils import get_files_in_drive_folder
import consts


def delete_drive_files():
    service = get_service()
    production_data_folder_ids = get_production_data_folder_ids(service, consts.PRODUCTION_FOLDERS)
    all_drive_files = {}
    for drive_folder_name, drive_folder_id in production_data_folder_ids.items():
        files = get_files_in_drive_folder(service, drive_folder_id)
        all_drive_files[drive_folder_name] = files

    for drive_folder in all_drive_files.keys():
        for file_ in all_drive_files[drive_folder]:
            drive_file_id = file_['id']
            drive_file_name = file_['name']

            print(f"Deleting file {drive_file_name} | id - {drive_file_id}")
            service.files().delete(fileId=drive_file_id).execute()


if __name__ == "__main__":
    delete_drive_files()
