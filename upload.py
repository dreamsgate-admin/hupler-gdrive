import os
import json
from copy import deepcopy

from apiclient import http

from utils import get_service, get_production_data_folder_ids
import consts


def _assert_config_file_exists():
    config_file_path = os.path.join('config.json')
    assert os.path.exists(config_file_path), "Config file doesn't exist"


def _assert_config_file_is_valid(config):

    config_file_keys_set = set(consts.CONFIG_FILE_KEYS)
    assert config_file_keys_set == set(config.keys()), \
        f"Config file is missing {config_file_keys_set.difference(set(config.keys()))}"

    use_root = config.pop('use_root')
    assert type(use_root) == bool, \
        f"TypeError detected in config file for {use_root}"

    for value in config.values():
        assert type(value) == str, \
            f"TypeError detected in config file for {value}"


def _get_source_folder_path(upload_config, source_folder_name):

    if upload_config['use_root']:
        source_folder_path = os.path.join(
            upload_config['data_folder_root'],
            source_folder_name
        )
    else:
        source_folder_path = os.path.join(source_folder_name)

    return source_folder_path


def _upload_all_files_in_folder(service, folder_id, source_folder_path):

    data_files = os.listdir(source_folder_path)

    for data_file in data_files:
        file_metadata = {
            'name': data_file,
            'parents': [folder_id]
        }
        full_path_to_file = os.path.join(source_folder_path, data_file)
        media = http.MediaFileUpload(
            full_path_to_file,
            mimetype='text/plain',
            resumable=True,
        )

        file = service.files().create(
            body=file_metadata,
            media_body=media,
            fields='id'
            ).execute()
        print(f"Uploaded file {data_file}. File ID is {file.get('id')}")


def upload_files():
    _assert_config_file_exists()
    with open(consts.CONFIG_FILE_PATH, 'r', encoding='utf-8') as fp:
        upload_config = json.load(fp)

    config = deepcopy(upload_config)
    _assert_config_file_is_valid(config)
    config = None

    service = get_service()
    production_data_folder_ids = get_production_data_folder_ids(service, consts.PRODUCTION_FOLDERS)

    # Contracts Data
    folder_id = production_data_folder_ids['ProductionJobsData']
    source_folder_name = upload_config['jobs_data_folder']
    source_folder_path = _get_source_folder_path(upload_config, source_folder_name)
    _upload_all_files_in_folder(service, folder_id, source_folder_path)

    # Tenders Data
    folder_id = production_data_folder_ids['ProductionTendersData']
    source_folder_name = upload_config['tenders_data_folder']
    source_folder_path = _get_source_folder_path(upload_config, source_folder_name)
    _upload_all_files_in_folder(service, folder_id, source_folder_path)

    # Social Feed Posts Data
    folder_id = production_data_folder_ids['ProductionSocialFeedPostsData']
    source_folder_name = upload_config['social_feed_posts_data_folder']
    source_folder_path = _get_source_folder_path(upload_config, source_folder_name)
    _upload_all_files_in_folder(service, folder_id, source_folder_path)


if __name__ == "__main__":
    upload_files()
