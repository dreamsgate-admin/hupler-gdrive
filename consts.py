import os

# If modifying these scopes, delete the file token.pickle.
SCOPES = [
    'https://www.googleapis.com/auth/drive.metadata.readonly',
    'https://www.googleapis.com/auth/drive.file',
    'https://www.googleapis.com/auth/drive.readonly',
]

CONFIG_FILE_PATH = os.path.join('config.json')

CONFIG_FILE_KEYS = (
    "jobs_data_folder",
    "tenders_data_folder",
    "social_feed_posts_data_folder",
    "data_folder_root",
    "use_root",
)

PRODUCTION_FOLDERS = [
    'ProductionJobsData',
    'ProductionTendersData',
    'ProductionSocialFeedPostsData'
]
