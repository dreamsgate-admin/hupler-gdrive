import os
import io

from apiclient import http

from utils import get_service, get_production_data_folder_ids
from utils import get_files_in_drive_folder
from cleanup import clean_json_data_folder
import consts


def _create_data_dirs(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


def download_files():
    print("Deleting existing json and csv files in json_data folder...")
    clean_json_data_folder()
    service = get_service()
    production_data_folder_ids = get_production_data_folder_ids(service, consts.PRODUCTION_FOLDERS)
    all_files = dict()
    for folder_name, folder_id in production_data_folder_ids.items():
        files = get_files_in_drive_folder(service, folder_id)
        all_files[folder_name] = files

    directory = os.path.join('json_data')
    _create_data_dirs(directory)

    for folder in all_files.keys():
        for file_ in all_files[folder]:
            file_id = file_['id']
            file_name = file_['name']

            file_path = os.path.join(directory, file_name)
            request = service.files().get_media(fileId=file_id)

            print(f"Downloading file {file_name} to directory {directory}")
            fh = io.FileIO(file_path, mode='wb')
            downloader = http.MediaIoBaseDownload(fh, request)
            done = False
            while done is False:
                status, done = downloader.next_chunk()
                print("Download %d%%." % int(status.progress() * 100))


if __name__ == "__main__":
    download_files()
